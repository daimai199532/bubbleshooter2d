using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
public class GameController 
{
    public static Action<bool,Vector3,ObjectBall?, ObjectBallCircle?> checkPositionBall;
    public static Action<Vector3, ObjectBallCircle, ObjectBall> getPositionBall;
    public static Action<GameObject> setPositionBall;
    //
    public static Action<ObjectBallCircle,TypeColorBall> updateBallInBallCircle;
    public static Action<ObjectBallCircle, bool> updateRoiBallCircle;
    //
    public static Action<List<ObjectBallCircle>, ObjectBallCircle> updateShowBallCircle;
    public static Action<int> updateScrollAreaEvent;
    public static void CheckPositionBall(bool isRayBall,Vector3 pos,ObjectBall? ball=null, ObjectBallCircle? ballCircleRay = null)//, ObjectBall? ball)
    {
        checkPositionBall?.Invoke(isRayBall,pos, ball, ballCircleRay);
        //Debug.Log(" run ============== MainModel");
    }
    public static void GetPositionBall(Vector3 pos, ObjectBallCircle ballCircle, ObjectBall ball)
    {
        getPositionBall?.Invoke(pos, ballCircle, ball);
    }
    public static void SetPositionBall(GameObject obj)
    {
        setPositionBall?.Invoke(obj);
    }
    public static void UpdateBallInBallCircle(ObjectBallCircle ballCircle, TypeColorBall colorBall)
    {
        updateBallInBallCircle?.Invoke(ballCircle,colorBall);
    }
    public static void UpdateRoiBallCircle(ObjectBallCircle ballCircle, bool isDeleteBall)
    {
        updateRoiBallCircle?.Invoke(ballCircle, isDeleteBall);
    }
    public static void UpdateShowBallCircle(List<ObjectBallCircle> list, ObjectBallCircle ball)
    {
        updateShowBallCircle?.Invoke(list, ball);
    }
    public static void UpdateScrollAreaEvent(int value)
    {
        updateScrollAreaEvent?.Invoke(value);
    }
}
