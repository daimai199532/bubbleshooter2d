using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LoadDataLevel : MonoBehaviour
{
    public TextAsset assetLevel;
    public ListBlock mylist = new ListBlock();
    public Area[] my = new Area[] { };
    private void Awake()
    {
        LoadData();
    }
    private void LoadData()
    {
        MainModel.listblock = new List<Area>();
        mylist = JsonUtility.FromJson<ListBlock> (assetLevel.text);
        //Debug.Log(mylist.listArea.Length + "=========LoadData========");
        //
        my = mylist.listArea;
        int count = my.Length - 1;

        for (int i = count; i > -1; i--)
        {
            Area area1 = new Area();
            area1.index = my[i].index;
            area1.color = my[i].color;
            area1.row = my[i].row;
            area1.countAreaInRow = my[i].countAreaInRow;
            MainModel.listblock.Add(area1);
            //Debug.Log(MainModel.listblock.Count + "=========LoadData========");
        }
        // my.listArea = MainModel.listblock.ToArray();
        //Debug.Log(my[count].index + "=========LoadData========" + MainModel.listblock[0].index);
    }
}
