using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainModel 
{
    public static List<Area> listblock;
}
[Serializable]
public class Area
{
    public int index;
    public string color;
    public int row;
    public int countAreaInRow;
}
[Serializable]
public class ListBlock
{
    public Area[] listArea;
}
