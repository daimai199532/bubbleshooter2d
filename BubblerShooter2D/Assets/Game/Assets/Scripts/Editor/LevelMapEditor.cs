using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LevelMapEditor : EditorWindow
{

   
    GUIStyle empty;
    Texture2D icon;
    Texture2D icon1;
    private Texture2D m_currentTexture;
    private stateColor m_currentColor = stateColor.image;
    private List<Node> m_listNode = new List<Node>();
    private List<ButtonBasic> m_list;
    private Vector2 scrollPos = Vector2.zero;
    private enum stateColor
    {
        image,
        image1
    }
  [MenuItem("Window/LevelMapEditor")]
    private static void OpenWindow()
    {
        LevelMapEditor window = GetWindow<LevelMapEditor>();
        window.titleContent = new GUIContent("Level Map Editor");
    }
    private void OnEnable()
    {
         icon = EditorGUIUtility.Load("Assets/Game/Assets/Texture2D/ball_addBlue.png") as Texture2D;
         icon1 = EditorGUIUtility.Load("Assets/Game/Assets/Texture2D/ball_addGreen.png") as Texture2D;
        //empty = new GUIStyle();
        empty = new GUIStyle();
        empty.normal.background = icon;
        //
        InitButton();
        //for (int i = 0; i < 15; i++)
        //{
        //    Debug.Log(m_list[i].x);
        //    //GUI.DrawTexture(new Rect(m_list[i].x, m_list[i].y, 50, 50), icon);
        //}
    }
    public int buttonCount = 5;
    public Button[] buttons;
    private void OnGUI()
    {

        //DrawNode();
        //ProcessGrid(Event.current);
        //GUILayout.Space(EditorGUIUtility.labelWidth);

        //UpdateIconList();
       
        //
        EditorGUILayout.BeginHorizontal();
        {

            GUILayout.Space(EditorGUIUtility.labelWidth); // tao 1 khoang trong
            //
            if (GUILayout.Button(icon, GUILayout.Width(100), GUILayout.Height(100)))
            {
                m_currentColor = stateColor.image;
            }
            //
            if (GUILayout.Button(icon1, GUILayout.Width(100), GUILayout.Height(100)))
            {
                m_currentColor = stateColor.image1;
            }

        }
        EditorGUILayout.EndHorizontal();
        //
        
        //
        m_listNode = new List<Node>();
        int next = 10;
        int positionAdd = 0;
        int count = 0;

        for (int i = 10; i > 0; i--)
        {

            for (int j = next; j > 0; j--)
            {
                count++;
                if (next == 10)
                    positionAdd = 25;
                else
                    positionAdd = 0;
                //
                //bt.Init(count, 100 + 500 - j * 50 + positionAdd, 300 + 500 - 50 * i, 0);
              
                if (j == 1)
                {

                    if (next == 10)
                        next = 9;
                    else
                        next = 10;
                    if (GUI.Button(new Rect(100 + 500 - j * 50 + positionAdd, 300 + 500 - 50 * i, 50, 50), "B" + i + "-" + j))
                    {
                        ButtonBasic bt1 = new ButtonBasic();
                        bt1.index = m_list[count - 1].index;
                        bt1.x = m_list[count - 1].x;
                        bt1.y = m_list[count - 1].y;
                        bt1.color = 2;
                        m_list.RemoveAt(count - 1);
                        m_list.Insert(count - 1, bt1);
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(100 + 500 - j * 50 + positionAdd, 300 + 500 - 50 * i, 50, 50), "B" + i + "-" + j))
                    {
                        ButtonBasic bt1 = new ButtonBasic();
                        bt1.index = m_list[count-1].index;
                        bt1.x = m_list[count-1].x;
                        bt1.y = m_list[count-1].y;
                        bt1.color = 2;
                        m_list.RemoveAt(count-1);
                        m_list.Insert(count-1, bt1);
                    }


                }
            }

        }
        for (int i = 0; i < 95; i++)
        {
            if(m_list[i].color > 0)
                GUI.DrawTexture(new Rect(m_list[i].x, m_list[i].y, 50, 50), icon);
        }
        //Debug.Log(m_list.Count);
    }
    public void MyButtonClickMethod(int buttonIndex)
    {
        Debug.Log("Button " + buttonIndex + " clicked!");
    }
    private void ChangeIconColor(stateColor color)
    {
        switch(color)
        {
            case stateColor.image:
                empty.normal.background = icon;
                m_currentTexture = icon;
                break;
            case stateColor.image1:
                empty.normal.background = icon1;
                m_currentTexture = icon1;
                break;
        }
        Debug.Log(m_currentTexture);
    }
    private void UpdateIconList()
    {
        
        for(int i =0; i< m_list.Count; i++)
        {
            //Debug.Log(m_list.Count);
            if(m_list[i].color > 1)
                GUI.DrawTexture(new Rect(m_list[i].x , m_list[i].x, 50, 50), icon);
        }
    }
    private void InitButton()
    {
        m_list = new List<ButtonBasic>();
        ButtonBasic bt;
        int next = 10;
        int positionAdd = 0;
        int count = 0;
        int x = 0;
        for (int i = 10; i > 0; i--)
        {

            for (int j = next; j > 0; j--)
            {
                bt = new ButtonBasic();
                count++;
                if (next == 10)
                    positionAdd = 25;
                else
                    positionAdd = 0;
                //
                if (j == 1)
                {

                    if (next == 10)
                        next = 9;
                    else
                        next = 10;
                  
                }
                x = 100 + 500 - j * 50 + positionAdd;
                //Debug.Log(x);
                bt.Init(count, x, 300 + 500 - 50 * i, 0);
                m_list.Add(bt);
            }

        }
    }
}
public class ButtonBasic
{
    public int index;
    public float x;
    public float y;
    public int color;
    public void Init(int id, float x1, float y1, int cl)
    {
        index = id;
        x = x1;
        y = y1;
        color = cl;
    }
}
