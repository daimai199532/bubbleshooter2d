using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    Rect rect;
    GUIStyle style;
    private int id = 0;
    //public Node(int id, Vector2 position, float width, float hight, GUIStyle defaultstyle)
    //{
    //    rect = new Rect(position.x, position.y, width, hight);
    //    style = defaultstyle;
    //}
    public void InitNode(int id,Rect rectValue, GUIStyle defaultstyle)
    {
        rect = rectValue;
        style = defaultstyle;
    }
    public void OnDrag(Vector2 delta)
    {
        rect.position += delta;
    }
    public void OnDraw()
    {
        Debug.Log(style);
        GUI.Box(rect, "", style);
    }
    public void SetStyle(GUIStyle NodeStyle)
    {
        style = NodeStyle;
    }
}
