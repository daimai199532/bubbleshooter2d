using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;


public class LevelEditor : EditorWindow
{
    GUIStyle empty;
    Texture2D iconBlue;
    Texture2D iconGreen;
    Texture2D iconEmpty;
    TextAsset textJson;
    private List<Area> m_listInit = new List<Area>();
    private ListArea<Area> m_listInitData;
    private string m_currentColor = "empty";
    private Area m_areaCu = new Area();
    string path;
    //
    [MenuItem("Window/LevelEditor")]
    private static void OpenWindow()
    {
        LevelEditor window = GetWindow<LevelEditor>();
        window.titleContent = new GUIContent("Level Editor");
    }
    private void OnEnable()
    {
        m_listInit = new List<Area>();
        //m_listInitData.listArea = new List<Area>();
        //
        iconBlue = EditorGUIUtility.Load("Assets/Game/Assets/Texture2D/ball_addBlue.png") as Texture2D;
        iconGreen = EditorGUIUtility.Load("Assets/Game/Assets/Texture2D/ball_addGreen.png") as Texture2D;
        iconEmpty = EditorGUIUtility.Load("Assets/Game/Assets/Texture2D/alert_icon.png") as Texture2D;
        textJson = EditorGUIUtility.Load("Assets/store1.json") as TextAsset;
        //empty = new GUIStyle();
        empty = new GUIStyle();
        empty.normal.background = iconEmpty;
        //
        //
        if (textJson != null)
            Debug.Log(" data: " + textJson);
        m_listInitData = JsonUtility.FromJson<ListArea<Area>>(textJson.text); // load data
        if (m_listInitData.listArea == null)
            Debug.Log("null init data");
        //
        CreateBlockEmpty();
        //
        UpdateGiaTriSauKhiLoad();
        //
    }

    private void OnGUI()
    {
        
        EditorGUILayout.BeginHorizontal();
        {

            GUILayout.Space(EditorGUIUtility.labelWidth); // tao 1 khoang trong
            //
            if (GUILayout.Button(iconBlue, GUILayout.Width(100), GUILayout.Height(100)))
            {
                //m_currentColor = stateColor.image;
                m_currentColor = "blue";
                
            }
            //
            if (GUILayout.Button(iconGreen, GUILayout.Width(100), GUILayout.Height(100)))
            {
                // m_currentColor = stateColor.image1;
                m_currentColor = "green";
            }
            if (GUILayout.Button("", GUILayout.Width(100), GUILayout.Height(100)))
            {
                // m_currentColor = stateColor.image1;
                m_currentColor = "empty";
                //path = EditorUtility.OpenFilePanel("show", "Assets/", "json");

            }
        }
        EditorGUILayout.EndHorizontal();
        //
        for (int j = 0; j < m_listInit.Count; j++)
        {
            if (GUI.Button(new Rect(m_listInit[j].x, m_listInit[j].y, 50, 50), "B" + 1 + "-" + j))
            {
                UpdateColorBlock(m_listInit[j], m_currentColor);
                SaveData();
            }
        }
        //for (int j = m_listInit.Count-1; j > -1; j--)
        //{
        //    if (GUI.Button(new Rect(m_listInit[j].x, m_listInit[j].y, 50, 50), "B" + 1 + "-" + j))
        //    {
        //        //UpdateColorBlock(m_listInit[j], m_currentColor);
        //        //SaveData();

        //    }
        //}
        //
        ShowAllBlock();
    }
    private void ShowAllBlock()
    {
        for (int j = 0; j < m_listInit.Count; j++)
        {
            ShowBlock(m_listInit[j]);
            Debug.Log(m_listInit[j].index + "===color ==" + m_listInit[j].color);
        }
    }

    private void SaveData()
    {

        List<Area> b = m_listInit;//new List<Area>() { };
        var data1 = new ListArea<Area>(b);
        var StringOut = JsonUtility.ToJson(data1);
        Debug.Log(b + "====" + StringOut);
        File.WriteAllText(Application.dataPath + "/store1.json", StringOut);
     
    }    
    private void UpdateGiaTriSauKhiLoad()
    {
       for(int i=0; i< m_listInit.Count; i++)
        {
            for(int j=0; j< m_listInitData.listArea.Count; j++)
            {
                if(m_listInit[i].index == m_listInitData.listArea[j].index && m_listInit[i].color != m_listInitData.listArea[j].color && m_listInitData.listArea[j].color != "empty")
                {
                    Debug.Log("Co thay doi colo truoc khi show===" + m_listInitData.listArea[j].color);
                    m_listInit[i].UpdateColor(m_listInitData.listArea[j].color);
                }
            }
        }
       
    }
 
    private void CreateBlockEmpty()
    {
      
        int next = 10;
        int positionAdd = 0;
        int count = 0;
        int x = 0;
        int row = 0;
        for (int i = 1; i < 11; i++)
        {
            row++;
            for (int j = 1; j < next + 1; j++)
            {

                count++;
                x = 100 + 500 - j * 50 + positionAdd;

                Area area = new Area();
                area.UpdateData(count, x, 300 + 500 - 50 * i, "empty",row, next);
                m_listInit.Add(area);
                //
            }
            if (next == 10)
            {
                next = 11;
                positionAdd = 25;
            }
            else
            {
                next = 10;
                positionAdd = 0;
            }
        }
    }
    private void UpdateColorBlock(Area area, string color)
    {
        area.UpdateColor(color);
    }
    private void ShowBlock(Area area)
    {
        //m_listInit.Add(m_currentColor);
        string color = area.color;
        switch (color)
        {
            case "empty":
                GUI.DrawTexture(new Rect(area.x, area.y, 50, 50), iconEmpty);
                break;
            case "blue":
                GUI.DrawTexture(new Rect(area.x, area.y, 50, 50), iconBlue);
                break;
            case "green":
                GUI.DrawTexture(new Rect(area.x, area.y, 50, 50), iconGreen);
                break;
        }
        //
       
    }
  
    [Serializable]
    public class ListArea<Area>
    {
        public List<Area> listArea;
        public ListArea(List<Area> data)
        {
            this.listArea = data;
        }
    }

}


[Serializable]
public class Area
{
    public int index;
    public int row;
    public float x;
    public float y;
    public string color;
    public int countAreaInRow;// dem so luong phan tu theo row Next
    public void UpdateData(int indexValue, float xValue, float yValue, string colorValue, int rowValue, int countAreaInRowValue)
    {
        index = indexValue;
        x = xValue;
        y = yValue;
        color = colorValue;
        row = rowValue;
        countAreaInRow = countAreaInRowValue;
    }
    public void UpdateColor(string colorValue)
    {
        color = colorValue;
    }
}

