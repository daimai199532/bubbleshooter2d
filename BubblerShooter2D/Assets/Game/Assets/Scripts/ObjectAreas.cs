﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectAreas : MonoBehaviour
{
    public ObjectBallCircle m_prefabsBallCircle;
    public Transform m_content;
    public List<ObjectBallCircle> m_listBalls = new List<ObjectBallCircle>();// list chinh
    public List<Area> m_listAreaTamThoi = new List<Area>();// list load tu data roi them
    public List<ObjectBallCircle> m_list = new List<ObjectBallCircle>();
    
    private int m_indexTargetPosition = -1;
    private int m_countDuyetXungQuanh = 0;
    public ObjectBallCircle m_ballCircleShowTarget;// vi tri ballCircle trước đó
    public Vector3 m_posShowTarget; // vi tri pos truoc do
    public int m_scrollDown = 0;
    // Start is called before the first frame update
    void Start()
    {
        DrawArea();
        GameController.checkPositionBall += CheckPositionBall;
        //
        GameController.updateShowBallCircle += ShowSprite;
        GameController.updateScrollAreaEvent += ScrollDown;
    }

    private void ScrollDown(int value)
    {
        float time = 0.5f / value;
        float defaultY = 0.5f;
        float posY = m_content.transform.position.y - (value * defaultY);
        m_content.DOMoveY(posY, time);
    }
    private void ShowSprite(List<ObjectBallCircle> list, ObjectBallCircle ball)
    {
        List<ObjectBallCircle> lists = list;
        for (int i=0; i< m_listBalls.Count;i++)
        {
            m_listBalls[i].ShowBall(m_listBalls[i], false, Color.white);
        }
        for(int i=0; i< lists.Count;i++)
        {
            lists[i].ShowBall(lists[i], true, Color.blue);
        }
        ball.ShowBall(ball, true, Color.red);
    }
    private void CheckPositionBall(bool isRayBall, Vector3 pos, ObjectBall ball,  ObjectBallCircle ballCircleRay)
    {
        //if (ballCircleRay == m_ballCircleShowTarget)
        //    return;
        //
        List<ObjectBallCircle> listTempty = new List<ObjectBallCircle>();
        int giatri = 0;
        int n = 0;
        Vector3 toaDo = Vector3.zero;
        if (ballCircleRay == null) // dang bi bug cho nay chua giai quyet xong
        {
            //if (pos == m_posShowTarget) return;
            //
            
            m_posShowTarget = pos;
            int rand = m_listBalls.Count - 1;
            float khoangCachGanNhat = Vector3.Distance(m_listBalls[rand].transform.position, pos);
            toaDo = m_listBalls[rand].transform.position;//listTempty[n].transform.position;
            float khoangCach = Vector3.Distance(m_listBalls[rand].transform.position, pos) ;
            giatri = rand;
            //Debug.Log(m_listBalls[rand] + "===============" + rand);
            while (m_listBalls[rand].GetIsBall())
            {
                rand = Random.RandomRange(0, m_listBalls.Count - 1);
                if (!m_listBalls[rand].GetIsBall())
                {
                    
                    khoangCachGanNhat = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                    toaDo = m_listBalls[rand].transform.position;
                    //khoangCach = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                    giatri = rand;
                    break;
                }
                
            }
            ////
            for (int i = 0; i < m_listBalls.Count; i++)
            {
                khoangCach = Vector3.Distance(m_listBalls[i].transform.position, (Vector3)pos);
                if (!m_listBalls[i].GetIsBall() && khoangCachGanNhat > khoangCach)
                {
                    //Debug.Log(khoangCachGanNhat +"====================="+ khoangCach);
                    khoangCachGanNhat = khoangCach;
                    toaDo = m_listBalls[i].transform.position;
                    giatri = i;
                    //break;
                }
            }
           
        }
        else
        {
            //List<ObjectBallCircle> listTempty = new List<ObjectBallCircle>();
            //m_ballCircleShowTarget = new ObjectBallCircle();

            if (ballCircleRay.m_objBotLeft != null && !ballCircleRay.m_objBotLeft.GetIsBall())
            {
                listTempty.Add(ballCircleRay.m_objBotLeft);
            }
            if (ballCircleRay.m_objBotRight != null && !ballCircleRay.m_objBotRight.GetIsBall())
            {
                listTempty.Add(ballCircleRay.m_objBotRight);
            }
            if (listTempty.Count < 1)
            {
                m_posShowTarget = pos;
                int rand = m_listBalls.Count - 1;
                float khoangCachGanNhat = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                toaDo = m_listBalls[rand].transform.position;//listTempty[n].transform.position;
                float khoangCach = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                giatri = rand;
                //Debug.Log(m_listBalls[rand] + "===============" + rand);
                while (m_listBalls[rand].GetIsBall())
                {
                    rand = Random.RandomRange(0, m_listBalls.Count - 1);
                    if (!m_listBalls[rand].GetIsBall())
                    {

                        khoangCachGanNhat = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                        toaDo = m_listBalls[rand].transform.position;
                        //khoangCach = Vector3.Distance(m_listBalls[rand].transform.position, pos);
                        giatri = rand;
                        break;
                    }

                }
                ////
                for (int i = 0; i < m_listBalls.Count; i++)
                {
                    khoangCach = Vector3.Distance(m_listBalls[i].transform.position, (Vector3)pos);
                    if (!m_listBalls[i].GetIsBall() && khoangCachGanNhat > khoangCach)
                    {
                        //Debug.Log(khoangCachGanNhat +"====================="+ khoangCach);
                        khoangCachGanNhat = khoangCach;
                        toaDo = m_listBalls[i].transform.position;
                        giatri = i;
                        //break;
                    }
                }
            }
            else
            {
                if (ballCircleRay.m_objRight != null && !ballCircleRay.m_objRight.GetIsBall())
                {
                    listTempty.Add(ballCircleRay.m_objRight);
                }
                if (ballCircleRay.m_objLeft != null && !ballCircleRay.m_objLeft.GetIsBall())
                {
                    listTempty.Add(ballCircleRay.m_objLeft);
                }

                //
                //int n = 0;//listTempty.Count - 1;
                //Debug.Log(n + "==================" + ballCircleRay.name);
                if (listTempty.Count < 1)
                    return;
                float khoangCachGanNhat = Vector3.Distance(listTempty[n].transform.position, pos);
                toaDo = listTempty[n].transform.position;
                float khoangCach = 0; //Vector3.Distance(m_listBalls[n].transform.position, pos) ;
                giatri = n;
                for (int i = 0; i < listTempty.Count; i++)
                {
                    khoangCach = Vector3.Distance(listTempty[i].transform.position, (Vector3)pos);
                    if (!listTempty[i].GetIsBall() && khoangCachGanNhat > khoangCach)
                    {
                        khoangCachGanNhat = khoangCach;
                        toaDo = listTempty[i].transform.position;
                        giatri = i;
                    }
                }

                for (int i = 0; i < m_listBalls.Count; i++)
                {

                    if (listTempty[giatri] == m_listBalls[i])
                    {
                        giatri = i;
                        break;
                    }
                }
            }
          
        }    
        //Debug.Log("Run======================"+ ball.name);
        
        GameController.GetPositionBall(m_listBalls[giatri].transform.position, m_listBalls[giatri], ball);
        ////
        if (m_indexTargetPosition == giatri)
            return;
        m_indexTargetPosition = giatri;
        m_ballCircleShowTarget = m_listBalls[giatri];
        // show area boom
        n = m_listBalls.Count - 1;
        for (int i = 0; i < n + 1; i++)
        {
            m_listBalls[i].ShowBall(m_listBalls[i], false);
        }

        m_listBalls[giatri].ShowBall(m_ballCircleShowTarget, true);
        //
        
    }
   
    private void CreateBallCircle2(int rowAdd) // user main
    {
        
        int countList = MainModel.listblock.Count;
        Debug.Log(countList);
        int index = (countList) - (5 * 10 + 5 * 11);//(countList) - (2 * 10 + 2 * 11);// lay 4 row dau tien
        int row = MainModel.listblock[index].row;
        int countArea = MainModel.listblock[index].countAreaInRow;
        int count = countArea;
        //Debug.Log(MainModel.listblock[countList - 1].index + "==================" + countArea);
        //
        for (int i = index; i < countList; i++)
        {
            if(count == 0)
            {
                countArea = MainModel.listblock[i].countAreaInRow;
                count = countArea;
            }
            Area area = new Area();
            area.index = count;
            area.color = MainModel.listblock[i].color;
            area.countAreaInRow = MainModel.listblock[i].countAreaInRow;
            area.row = MainModel.listblock[i].row + rowAdd;
            m_listAreaTamThoi.Add(area);
            count--;
            //m_listAreaTamThoi.Add(MainModel.listblock[i]);
        }

        CreateRowAdd(rowAdd, countArea);
    }
    private void CreateRowAdd(int rowAdd, int nextCountRowBefore)
    {
        int nextCount = 9;
        // check row trc do
        // tinh so countNext cua tung dong
      
        if (nextCountRowBefore == 10)
        {
            nextCount = 11;
        }
        else
        {
            nextCount = 10;
        }
        //
        //Debug.Log(nextCountRowBefore + "===========" + nextCount);
        List<int> listCountNext = new List<int>();// nextCount theo row
        int countAreaNew = 0; // so phan can tao ra
        for (int i=0; i< rowAdd;i++)
        {
            listCountNext.Add(nextCount);
            countAreaNew += nextCount;
            // tinh so countNext cua tung dong
            if (nextCount == 10)
            {
                nextCount = 11;
            }
            else
            {
                nextCount = 10;
            }
        }
        //
        //foreach (int i in listCountNext)
        //{
        //    Debug.Log(i + "====================row");
        //}
        //listCountNext.Reverse();
        //
        //Debug.Log(countAreaNew + "====================SLL");
        int rowCount = 0;
        int countNext = listCountNext[rowCount];
        int count = 1;
        for(int i = 0;i < countAreaNew; i++ )
        {
           
            Area area = new Area();
            area.index = (countNext+1)-count;
            area.color = "empty";
            area.countAreaInRow = countNext;
            area.row = rowAdd - rowCount;
            m_listAreaTamThoi.Add(area);
            count++;
            if (count == countNext+1)
            {
                //Debug.Log(rowCount + "--row udpate bẻoe--" + countAreaNew);
                count = 1;
                rowCount+=1;
                if (rowCount < listCountNext.Count)
                    countNext = listCountNext[rowCount];
                //Debug.Log(rowCount + "--row udpate--" + countAreaNew);
            }
        }
        //Debug.Log(countAreaNew + "====================");
    }
    private void DrawArea()
    {
        CreateBallCircle2(2);
        m_listBalls = new List<ObjectBallCircle>();
        //m_list = m_listAreaTamThoi;
        int countList = m_listAreaTamThoi.Count;
        int countArea = m_listAreaTamThoi[0].countAreaInRow;
        int index = 0;
        //int count = 0;
        float posAddY = 0;
        float posAddX = -.25f;
        //
        if (countArea == 10)
        {
            posAddX = -0.5f;
            // Debug.Log("ok 11111111111");
        }
        else
        {
            posAddX = -.25f;
        }
        //
        for (int i = index; i < countList; i++)
        {

            if (countArea != m_listAreaTamThoi[i].countAreaInRow)
            {
                countArea = m_listAreaTamThoi[i].countAreaInRow;
                posAddY -= 0.44f;
                if (countArea == 10)
                {
                    posAddX = -0.5f;
                    // Debug.Log("ok 11111111111");
                }
                else
                {
                    posAddX = -.25f;
                }
            }

            ObjectBallCircle obj = Instantiate(m_prefabsBallCircle);
            obj.transform.SetParent(m_content);
            obj.transform.position = new Vector3((2.25f + posAddX) - ((m_listAreaTamThoi[i].index-2) * 0.5f), 4.5f + posAddY, 0);
            obj.Init(m_listAreaTamThoi[i].row, m_listAreaTamThoi[i].index, m_listAreaTamThoi[i].countAreaInRow);
            //Debug.Log(m_listAreaTamThoi[i].color);
            obj.name = "ball-circle-"+ m_listAreaTamThoi[i].row + "-"+ m_listAreaTamThoi[i].index;
            obj.CreateBall(m_listAreaTamThoi[i].color);
            obj.gameObject.SetActive(true);
            m_listBalls.Add(obj);
            //count++;
        }
        //
        m_listBalls.Reverse();

        for (int i = 0; i < m_listBalls.Count; i++)
        {
            Tinh1(m_listBalls[i].m_row, m_listBalls[i].m_index, m_listBalls[i].m_countIndex, i, m_listBalls[m_listBalls.Count-1].m_row);
        }

    }
    private void Tinh1(int row, int index, int countIndex, int i, int rowLast) // i == vi tri trong list
    {
        ObjectBallCircle objLeft = null;
        ObjectBallCircle objRight = null;
        ObjectBallCircle objTopLeft = null;
        ObjectBallCircle objTopRight = null;
        ObjectBallCircle objBotLeft = null;
        ObjectBallCircle objBotRight = null;
        if(i-1 > 0)
        {
            if (m_listBalls[i].m_row == m_listBalls[i -1].m_row)
                objRight = m_listBalls[i - 1];
        }
        if (index < countIndex)
        {
            if (m_listBalls[i].m_row == m_listBalls[i + 1].m_row)
                objLeft = m_listBalls[i+ 1];
        }
        if(countIndex == 10)
        {
            if (m_listBalls[i].m_row == 1) // chi co top
            {
                objTopLeft = m_listBalls[i + 11];// = m_list[i + countIndex + 1];
                objTopRight = m_listBalls[i + 10];
            }
            else if (m_listBalls[i].m_row == rowLast) // just have bot
            {
                objBotRight = m_listBalls[i - 11];
                objBotLeft = m_listBalls[i - 10];
            }
            else
            {
                objTopLeft = m_listBalls[i + 11];
                objTopRight = m_listBalls[i + 10];
                objBotRight = m_listBalls[i - 11];
                objBotLeft = m_listBalls[i - 10];
            }

        }
        else
        {
            //Debug.Log("run =========================" + rowLast);
            if (m_listBalls[i].m_row == 1) // row first
            {
                if (m_listBalls[i].m_index ==1)
                {
                    objTopLeft = m_listBalls[i + 12];
                }
                else if (m_listBalls[i].m_index == countIndex)
                {
                    objTopRight = m_listBalls[i + 11];
                }
                else
                {
                    objTopLeft = m_listBalls[i + 12];
                    objTopRight = m_listBalls[i + 11];
                }
                
            }
            else if (m_listBalls[i].m_row == rowLast) // row last
            {
                
                if (m_listBalls[i].m_index == 1)
                {
                    objBotLeft = m_listBalls[i - 10];
                }
                else if (m_listBalls[i].m_index == countIndex)
                {
                    objBotRight = m_listBalls[i - 11];
                }
                else
                {
                    objBotRight = m_listBalls[i - 11];
                    objBotLeft = m_listBalls[i - 10];
                }
            }
            else
            {
                if (m_listBalls[i].m_index == 1)
                {
                    objBotLeft = m_listBalls[i - 10];
                    objTopLeft = m_listBalls[i + 11];
                }
                else if (m_listBalls[i].m_index == countIndex)
                {
                    objTopRight = m_listBalls[i + 10];
                    objBotRight = m_listBalls[i - 11];
                }
                else
                {
                    objBotLeft = m_listBalls[i - 10];
                    objTopLeft = m_listBalls[i + 11];
                    objTopRight = m_listBalls[i + 10];
                    objBotRight = m_listBalls[i - 11];
                }
            }
        }
        m_listBalls[i].InitNextObj(objLeft, objRight, objTopLeft, objTopRight, objBotLeft, objBotRight, row, index, countIndex, i, rowLast);
        //m_listBalls[i].CreateBall(m_listBalls[i].m_typeColorBall);
    }
    
}
