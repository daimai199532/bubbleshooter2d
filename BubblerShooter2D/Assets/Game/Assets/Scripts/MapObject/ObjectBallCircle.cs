﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectBallCircle : MonoBehaviour
{
    [SerializeField] private Collider2D m_collider;
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    [SerializeField] private SpriteRenderer m_spriteRenderer1;
    public ObjectBall m_objBallPrefabs;
    public ObjectBall m_objCurrentBall;
    public ObjectBallCircle m_objLeft;
    public ObjectBallCircle m_objRight;
    public ObjectBallCircle m_objTopLeft;
    public ObjectBallCircle m_objTopRight;
    public ObjectBallCircle m_objBotLeft;
    public ObjectBallCircle m_objBotRight;
    public int m_row = 0;
    public int m_index = 0;
    public int m_indexLeft = 0;
    public int m_indexRight = 0;
    public int m_indexTopRight = 0;
    public int m_indexTopLeft = 0;
    public int m_indexBotRight = 0;
    public int m_indexBotLeft = 0;

    public int m_countIndex = 0;
    public bool m_isBall = false;
 
    public List<ObjectBallCircle> m_listNextBall;
    public TypeColorBall m_typeColorBall = TypeColorBall.empty;
    public List<ObjectBallCircle> m_listCanXoa = new List<ObjectBallCircle>();
    //
    public List<ObjectBallCircle> m_listBallCircleHangTren = new List<ObjectBallCircle>();
    public List<ObjectBallCircle> m_listBallCircleHangTrenNoEmpty = new List<ObjectBallCircle>();
    public List<ObjectBallCircle> m_listBallLienKetHangTren = new List<ObjectBallCircle>();
    //
    public List<ObjectBallCircle> m_listtDaaDuyetBiiRot = new List<ObjectBallCircle>();
    public List<ObjectBallCircle> m_listBiiRot = new List<ObjectBallCircle>();
    //

    //
    private ObjectBallCircle m_ballCircle;
    //
    //check data
    //(int row, int index, int countIndex, int i, int rowLast) // i == vi tri trong list
    public int m_indexTest = 0;
    public int m_countIndexTest = 0;
    public int m_rowTest = 0;
    public int m_iTest = 0;
    public int m_rowLastTest = 0;
    public string m_colorInit = "empty";

    private int m_rowTop = 0;
    private void Awake()
    {
      
        GameController.updateRoiBallCircle += UpdateRoiBall;
        //
        m_ballCircle = GetComponent<ObjectBallCircle>();

    }
    private void OnDestroy()
    {
      
        GameController.updateRoiBallCircle -= UpdateRoiBall;
    }
    public void InitNextObj(ObjectBallCircle? left, ObjectBallCircle? right, ObjectBallCircle? topLeft, ObjectBallCircle? topRight, ObjectBallCircle? botLeft, ObjectBallCircle? botRight, int rowtest, int indextest, int countIndextest, int itest, int rowLasttest)
    {
        m_rowTest = rowtest;
        m_indexTest = indextest;
        m_iTest = itest;
        m_countIndexTest = countIndextest;
        m_rowLastTest = rowLasttest;

        //
        m_listNextBall = new List<ObjectBallCircle>();
        m_objLeft = left;
        m_objRight = right;
        m_objTopLeft = topLeft;
        m_objTopRight = topRight;
        m_objBotLeft = botLeft;
        m_objBotRight = botRight;
        if (m_objLeft != null)
            if (!m_listNextBall.Contains(m_objLeft))
                m_listNextBall.Add(m_objLeft);
        if (m_objRight != null)
            if (!m_listNextBall.Contains(m_objRight))
                m_listNextBall.Add(m_objRight);
        if (m_objTopLeft != null)
            if (!m_listNextBall.Contains(m_objTopLeft))
                m_listNextBall.Add(m_objTopLeft);
        if (m_objTopRight != null)
            if (!m_listNextBall.Contains(m_objTopRight))
                m_listNextBall.Add(m_objTopRight);
        if (m_objBotLeft != null)
            if (!m_listNextBall.Contains(m_objBotLeft))
                m_listNextBall.Add(m_objBotLeft);
        if (m_objBotRight != null)
            if (!m_listNextBall.Contains(m_objBotRight))
                m_listNextBall.Add(m_objBotRight);
    }
    public void Init(int row, int index, int countIndex)
    {
        //m_collider.enabled = false;
        m_row = row;
        m_index = index;
        m_countIndex = countIndex;
        //
        if (m_row < 2)// dong 1 thi bot = 0;
        {
            if (m_index < 2 || m_index == m_countIndex)
            {
                if (m_index < 2) // kiem tra ben trai
                    m_indexLeft = m_index + 1;
                if (m_index == m_countIndex) // kiem tra ben phai
                    m_indexRight = m_index - 1;
            }
            else
            {
                m_indexLeft = m_index + 1;
                m_indexRight = m_index;
            }
            m_indexTopLeft = m_index + 1;
            m_indexTopRight = m_index;
        }
        else
        {
            if (m_index < 2 || m_index == m_countIndex) //
            {
                if (m_index < 2) // kiem tra ben trai
                {
                    m_indexLeft = m_index + 1;

                    m_indexTopLeft = m_index;
                    m_indexBotLeft = m_index;
                    if (m_countIndex == 10)
                    {
                        m_indexTopLeft = m_index + 1;
                        m_indexBotLeft = m_index + 1;
                        m_indexTopRight = m_index;
                        m_indexBotRight = m_index;
                    }

                }
                if (m_index == m_countIndex) // kiem tra ben phai
                {
                    m_indexRight = m_index - 1;
                    if (m_countIndex == 10)
                    {
                        m_indexTopLeft = m_index + 1;
                        m_indexBotLeft = m_index + 1;
                        //
                        m_indexTopRight = index;
                        m_indexBotRight = index;
                    }
                    else
                    {
                        m_indexTopRight = index - 1;
                        m_indexBotRight = index - 1;
                    }

                }

            }
            else
            {
                m_indexLeft = m_index + 1;
                m_indexRight = m_index;
                //
                m_indexTopLeft = index;
                m_indexBotLeft = index;
                //
                m_indexTopRight = index - 1;
                m_indexBotRight = index - 1;
                if (m_countIndex == 10)
                {
                    m_indexTopLeft = index + 1;
                    m_indexBotLeft = index + 1;
                    //
                    m_indexTopRight = index;
                    m_indexBotRight = index;
                }
            }

        }
    }
    public NextPos GetNextIndexBall()
    {
        NextPos next = new NextPos();
        next.row = m_row;
        next.index = m_index;
        next.indexLeft = m_indexLeft;
        next.indexRight = m_indexRight;
        next.indexTopRight = m_indexTopRight;
        next.indexTopLeft = m_indexTopLeft;
        next.indexBotRight = m_indexBotRight;
        next.indexBotLeft = m_indexBotLeft;

        return next;
    }
    public void ShowBall(ObjectBallCircle ballCircle, bool isShow)
    {
        //Debug.Log("show ball============");
        //if (ballCircle != m_ballCircle)
        //    return;
        if (isShow)
            m_spriteRenderer.enabled = true;
        else
            m_spriteRenderer.enabled = false;
    }
    public void ShowBall(ObjectBallCircle ballCircle, bool isShow, Color color)
    {
       
        //if (ballCircle != m_ballCircle)
        //    return;
        if (isShow)
        {
            m_spriteRenderer1.enabled = true;
            m_spriteRenderer1.color = color;
            //Debug.Log("show ball============" + ballCircle.name);
        }
        else
        {
            m_spriteRenderer1.enabled = false;
           
        }
    }
   
    public bool GetIsBall()
    {
        return m_isBall;
    }
    public TypeColorBall GetTypeColorBall()
    {
        return m_typeColorBall;
    }
    public void DestroyCurrentBall()
    {
        Destroy(m_objCurrentBall.gameObject);
        m_isBall = false;
        m_objCurrentBall = null;
        m_typeColorBall = TypeColorBall.empty;
        if (m_collider.gameObject.activeSelf)
            m_collider.enabled = false;
    }
    public void SetBall(ObjectBallCircle ballCircle, ObjectBall ball, TypeColorBall colorBall, bool isUpdate)
    {
        m_isBall = true;
        m_objCurrentBall = ball;
        if (m_collider != null)
            m_collider.enabled = true;
        m_typeColorBall = ball.GetTypeBall();
        if (isUpdate)
            UpdateBall(ballCircle, colorBall);


    }
    private void UpdateRoiBall(ObjectBallCircle objectBallCircle, bool isDelete)
    {
        if (objectBallCircle != m_ballCircle)
            return;
        //
        if(isDelete)
        {
            DestroyCurrentBall();
        }
        else
        {
            if (m_objCurrentBall == null)
                return;
            m_objCurrentBall.transform.DOMoveY(0, 0.5f).OnComplete(() => {
                DestroyCurrentBall();
            });
        }
    }
    private void UpdateBall(ObjectBallCircle ballCircle, TypeColorBall colorBall) // add TypeColor
    {
        m_listCanXoa = new List<ObjectBallCircle>();
   
        if (ballCircle != m_ballCircle)
            return;

        m_listCanXoa.Add(ballCircle);

        for (int i = 0; i < m_listCanXoa.Count; i++)
        {
            DuyeCacBallCungColor(m_listCanXoa[i]);
        }

        //
        DuyetListBallXoa();
        //
      
    }
    private void DuyeCacBallCungColor(ObjectBallCircle objec) // Duyệt các ô trống không chứa ball -> kiểm tra các ô xung quanh ô trống liên kết với ô trống -> thường là các ô dưới các ô trống 
    {
       
        if (objec.m_objRight != null && objec.m_objRight.GetIsBall() && objec.m_typeColorBall == objec.m_objRight.m_typeColorBall && !m_listCanXoa.Contains(objec.m_objRight))
        {
            //Debug.Log(objec.m_objRight.GetIsBall() + )
            m_listCanXoa.Add(objec.m_objRight);
        }
        if (objec.m_objLeft != null && objec.m_objLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objLeft) && objec.m_typeColorBall == objec.m_objLeft.m_typeColorBall)
        {
            m_listCanXoa.Add(objec.m_objLeft);
        }
        if (objec.m_objBotLeft != null && objec.m_objBotLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotLeft) && objec.m_typeColorBall == objec.m_objBotLeft.m_typeColorBall)
        {
            m_listCanXoa.Add(objec.m_objBotLeft);
        }
        if (objec.m_objBotRight != null && objec.m_objBotRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotRight) && objec.m_typeColorBall == objec.m_objBotRight.m_typeColorBall)
        {
            m_listCanXoa.Add(objec.m_objBotRight);
        }
        if (objec.m_objTopLeft != null && objec.m_objTopLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objTopLeft) && objec.m_typeColorBall == objec.m_objTopLeft.m_typeColorBall)
        {
            m_listCanXoa.Add(objec.m_objTopLeft);
        }

        if (objec.m_objTopRight != null && objec.m_objTopRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objTopRight) && objec.m_typeColorBall == objec.m_objTopRight.m_typeColorBall)
        {
            m_listCanXoa.Add(objec.m_objTopRight);
        }
    }
    private void DuyetListBallXoa() // list empty 1
    {
        if (m_listCanXoa.Count < 3)
            return;
        //
        DuyetBallConLai();
        //
        DuyetAllLienKetDuoiListCanXoa();
        //
        m_listBiiRot = m_listtDaaDuyetBiiRot;
        //
        GameController.UpdateShowBallCircle(m_listBiiRot, m_ballCircle);
        //
        ChuanBiXoa();

    }
    private void ChuanBiXoa()
    {
        StartCoroutine(WaitChuanBiXoa());
    }
    private IEnumerator WaitChuanBiXoa()
    {
        yield return new WaitForSeconds(4f);
        if (m_listCanXoa.Count > 2)  // xoa cac ball
        {
            for (int i = 0; i < m_listCanXoa.Count; i++)
            {
                GameController.UpdateRoiBallCircle(m_listCanXoa[i], true);
            }
            //m_listCanXoa.Clear();
        }
        yield return new WaitForSeconds(1f);
        if (m_listBiiRot.Count > 0)
        {
            for (int i = 0; i < m_listBiiRot.Count; i++)
            {
                GameController.UpdateRoiBallCircle(m_listBiiRot[i], false);
                //ballCir.DestroyCurrentBall();
            }
            //m_listBiiRot.Clear();
        }
    }

 
    private void DuyetBallConLai()
    {
        if (m_listCanXoa.Count < 3)
            return;
        // 
        List<ObjectBallCircle> listTop = new List<ObjectBallCircle>();
        //
        m_listBallCircleHangTren = new List<ObjectBallCircle>(); ;
        //
        DuyetHangTrenBallCircle();
        //
      
    }
    
    // tat ca cac ball lien ket voi listBallCircleHangTren -> duyet cac ball lien ket voi ball != empty
    private void DuyetHangTrenBallCircle()
    {
        ObjectBallCircle obj = new ObjectBallCircle();
        int countListCanXoa = m_listCanXoa.Count;
        //
        obj = m_listCanXoa[0];
       
        for (int i = 0; i < countListCanXoa; i++)
        {
            if (m_listCanXoa[i].m_row > obj.m_row )//&& m_listCanXoa[i].m_objTopLeft != null && m_listCanXoa[i].m_objTopLeft.GetIsBall() || m_listCanXoa[i].m_objTopRight != null && m_listCanXoa[i].m_objTopRight.GetIsBall())//(m_rowTop - 1))
            {
                obj = m_listCanXoa[i];
              
            }
        }
        //
        if (obj.m_objTopLeft != null)
        {
            obj = obj.m_objTopLeft;
        }
        else if (obj.m_objTopRight != null)
        {
            obj = obj.m_objTopRight;
        }
        m_rowTop = obj.m_row;
        m_listBallCircleHangTren.Add(obj);
        //
        for (int i = 0; i < m_listBallCircleHangTren.Count; i++)
        {
            DuyetLeftRightNoNullListHangTren(m_listBallCircleHangTren[i]);
        }
        //GameController.UpdateShowBallCircle(m_listBallCircleHangTren, m_ballCircle);
        m_ballCircle.name = "check======================";
        //
        m_listBallCircleHangTrenNoEmpty = new List<ObjectBallCircle>();
        for (int i = 0; i < m_listBallCircleHangTren.Count; i++)
        {
            if (m_listBallCircleHangTren[i].GetIsBall() && !m_listCanXoa.Contains(m_listBallCircleHangTren[i]))
                m_listBallCircleHangTrenNoEmpty.Add(m_listBallCircleHangTren[i]);
        }
        //GameController.UpdateShowBallCircle(m_listBallCircleHangTrenNoEmpty, m_ballCircle);
 
        m_listBallLienKetHangTren = m_listBallCircleHangTrenNoEmpty;
        //
        DuyetTatCaCBallDuoiHangTren();
    }
    private void DuyetLeftRightNoNullListHangTren(ObjectBallCircle objec)
    {
        if(objec.m_objLeft != null )//&& !m_listCanXoa.Contains(objec.m_objLeft) /*&& !m_listBallCircleEmpty.Contains(objec.m_objLeft)*/)
        {
            if (!m_listBallCircleHangTren.Contains(objec.m_objLeft))
                m_listBallCircleHangTren.Add(objec.m_objLeft);
        }
        if (objec.m_objRight != null)// && !m_listCanXoa.Contains(objec.m_objRight)/* && !m_listBallCircleEmpty.Contains(objec.m_objRight)*/)
        {
            if (!m_listBallCircleHangTren.Contains(objec.m_objRight))
                m_listBallCircleHangTren.Add(objec.m_objRight);
        }

    }
    private void DuyetTatCaCBallDuoiHangTren()
    {
        for(int i=0; i< m_listBallLienKetHangTren.Count; i++)
        {
            DuyetBotLeftRightDuoiHangTren(m_listBallLienKetHangTren[i]);
        }
        //
        for (int i = 0; i < m_listBallLienKetHangTren.Count; i++)
        {
            DuyetLeftRightAndBotDuoiHangTren(m_listBallLienKetHangTren[i]);
        }
        // duyet tat ca ball duoi listcanxoa -> neu ko lien ket voi listLienKetHangTren thi dua vao listbiroi
    }
    
    private void DuyetBotLeftRightDuoiHangTren(ObjectBallCircle objec)
    {
        if (objec.m_objBotRight != null && objec.m_objBotRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotRight) && !m_listBallLienKetHangTren.Contains(objec.m_objBotRight))
            m_listBallLienKetHangTren.Add(objec.m_objBotRight);
        if (objec.m_objBotLeft != null && objec.m_objBotLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objBotLeft))
            m_listBallLienKetHangTren.Add(objec.m_objBotLeft);
    }
    private void DuyetLeftRightAndBotDuoiHangTren(ObjectBallCircle objec)
    {
        if (objec.m_objLeft != null && objec.m_objLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objLeft))
            m_listBallLienKetHangTren.Add(objec.m_objLeft);
        if (objec.m_objRight != null && objec.m_objRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objRight) && !m_listBallLienKetHangTren.Contains(objec.m_objRight))
            m_listBallLienKetHangTren.Add(objec.m_objRight);
        if (objec.m_objBotRight != null && objec.m_objBotRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotRight) && !m_listBallLienKetHangTren.Contains(objec.m_objBotRight))
            m_listBallLienKetHangTren.Add(objec.m_objBotRight);
        if (objec.m_objBotLeft != null && objec.m_objBotLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objBotLeft))
            m_listBallLienKetHangTren.Add(objec.m_objBotLeft);
    }
    private void DuyetAllLienKetDuoiListCanXoa()
    {
        m_listtDaaDuyetBiiRot = new List<ObjectBallCircle>();
        // Add vao m_listtDaaDuyetBiiRot
        for (int i=0; i< m_listCanXoa.Count; i++) 
        {
            //DuyetBotLeftRightListCanXoa(m_listCanXoa[i]);
            DuyetLeftRightAndBotListXoa(m_listCanXoa[i]);
        }
        //
        for(int i=0; i< m_listtDaaDuyetBiiRot.Count; i++)
        {
            DuyetLeftRightAndBotListXoa(m_listtDaaDuyetBiiRot[i]);
        }
   
    }

    private void DuyetLeftRightAndBotListXoa(ObjectBallCircle objec)
    {
        if (objec.m_objLeft != null && objec.m_objLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objLeft) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objLeft))
            m_listtDaaDuyetBiiRot.Add(objec.m_objLeft);
        if (objec.m_objRight != null && objec.m_objRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objRight) && !m_listBallLienKetHangTren.Contains(objec.m_objRight) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objRight))
            m_listtDaaDuyetBiiRot.Add(objec.m_objRight);
        if (objec.m_objBotLeft != null && objec.m_objBotLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objBotLeft) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objBotLeft))
            m_listtDaaDuyetBiiRot.Add(objec.m_objBotLeft);
        if (objec.m_objBotRight != null && objec.m_objBotRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objBotRight) && !m_listBallLienKetHangTren.Contains(objec.m_objBotRight) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objBotRight))
            m_listtDaaDuyetBiiRot.Add(objec.m_objBotRight);
        ////

        if (objec.m_objTopLeft != null && objec.m_objTopLeft.m_row < m_rowTop && objec.m_objTopLeft.GetIsBall() && !m_listCanXoa.Contains(objec.m_objTopLeft) && !m_listBallLienKetHangTren.Contains(objec.m_objTopLeft) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objTopLeft))
            m_listtDaaDuyetBiiRot.Add(objec.m_objTopLeft);
        if (objec.m_objTopRight != null && objec.m_objTopRight.m_row < m_rowTop  && objec.m_objTopRight.GetIsBall() && !m_listCanXoa.Contains(objec.m_objTopRight) && !m_listBallLienKetHangTren.Contains(objec.m_objTopRight) && !m_listtDaaDuyetBiiRot.Contains(objec.m_objTopRight))
            m_listtDaaDuyetBiiRot.Add(objec.m_objTopRight);
    }
    public void CreateBall(string color)
    {
        GetTypeColorBall(color);
        if (m_colorInit != color)
        {
            if (m_collider != null)
                m_collider.enabled = true;
            m_colorInit = color;
            //
            ObjectBall obj = Instantiate(m_objBallPrefabs);
            m_objCurrentBall = obj;
            obj.transform.SetParent(transform);
            obj.transform.position = transform.position;
            obj.Init(m_typeColorBall);
            m_isBall = true;
            obj.gameObject.SetActive(true);
            //
            m_isBall = true;
            m_typeColorBall = obj.GetTypeBall();
            
            //Debug.Log("================" + m_objCurrentBall.name);
        }
      
    }
    private TypeColorBall GetTypeColorBall(string color)
    {
       
        switch (color)
        {
            case "empty":
                m_typeColorBall = TypeColorBall.empty;
                break;
            case "blue":
                m_typeColorBall = TypeColorBall.blue;
                break;
            case "green":
                m_typeColorBall = TypeColorBall.green;
                break;
        }
        return m_typeColorBall;
    }
   
}
[SerializeField]
public enum TypeColorBall
{
    empty = -1,
    blue = 0,
    red = 1,
    green = 2,
    yellow= 3,
    violet = 4
}
