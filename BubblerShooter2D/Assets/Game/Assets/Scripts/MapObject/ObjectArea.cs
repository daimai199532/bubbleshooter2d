using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectArea : MonoBehaviour
{
    [SerializeField] private List<ObjectBallCircle> m_listBalls;
    private List<ObjectBallCircle> m_listNextObjs;
    private ObjectArea m_area;
    private int m_indexTargetPosition = -1;
    private int m_countDuyetXungQuanh = 0;
    public List<ObjectBallCircle> m_listShow;
    private void Awake()
    {
        m_area = GetComponent<ObjectArea>();
        Init();
        GameController.checkPositionBall += CheckPositionBall;
    }
    private void OnDestroy()
    {
        GameController.checkPositionBall -= CheckPositionBall;
    }
    private void Init()
    {
        int rowCurrent = 1;
        int count = 0;

        int countRow1 = 10;
        int countRow2 = 11;
        //
        int countNext = countRow1;
        foreach (ObjectBallCircle obj in m_listBalls)
        {
            count++;
            if (count == countNext)
            {
                obj.Init(rowCurrent, count, countNext);
                count = 0;
                rowCurrent++;
                if (countNext == countRow1)
                {
                    countNext = countRow2;
                }
                else
                {
                    countNext = countRow1;
                }
            }
            else
            {
                obj.Init(rowCurrent, count, countNext);
            }
        }
        //
        Duyet();
    }
    private void Duyet()
    {
        int rowCurrent = 1;
        int count = 0;

        int countRow1 = 10;
        int countRow2 = 11;
        //
        int countNext = countRow1;
        int n = m_listBalls.Count;
        for (int i = 0; i < n; i++)
        {
            count++;
            if (count == countNext)
            {
                Tinh(rowCurrent, count, countNext, i,4); // i == vi tri trong list
              
                count = 0;
                rowCurrent++;
                if (countNext == countRow1)
                {
                    countNext = countRow2;
                }
                else
                {
                    countNext = countRow1;
                }
            }
            else
            {
                Tinh(rowCurrent, count, countNext, i, 4);
               
            }
           
        }
    }
   
    private void CheckPositionBall(bool isRayBall, Vector3 pos,ObjectBall ball,  ObjectBallCircle ballCircleRay)
    {
        if (pos == null)
            pos = Vector3.zero;
        //Debug.Log("Run======================"+isRayBall);
        int n = m_listBalls.Count - 1;
        float khoangCachGanNhat = Vector3.Distance(m_listBalls[n].transform.position, (Vector3)pos) + 1f;
        Vector3 toaDo = m_listBalls[n].transform.position;
        float khoangCach = 0; //Vector3.Distance(m_listBalls[n].transform.position, pos) ;
        int giatri = n;
        
        for (int i = 0; i < m_listBalls.Count; i++)
        {
            khoangCach = Vector3.Distance(m_listBalls[i].transform.position, (Vector3)pos);
            if (!m_listBalls[i].GetIsBall() && khoangCachGanNhat > khoangCach)
            {
                khoangCachGanNhat = khoangCach;
                toaDo = m_listBalls[i].transform.position;
                giatri = i;
            }
        }
     
        ////Debug.Log(giatri + "=========" + toaDo);
        GameController.GetPositionBall(toaDo, m_listBalls[giatri], ball);
        //
        if (m_indexTargetPosition == giatri)
            return;
        m_indexTargetPosition = giatri;

        // show area boom
        for (int i = 0; i < n + 1; i++)
        {
            m_listBalls[i].ShowBall(m_listBalls[i], false);
        }

        m_listBalls[giatri].ShowBall(m_listBalls[giatri], true);
        //
        UpdateShowBall(m_listBalls[giatri], 0);
    }
    public void UpdateShowBall(ObjectBallCircle ballCircle, int lastCount)
    {
        //List<ObjectBallCircle> listShow = new List<ObjectBallCircle>();
        m_listShow = new List<ObjectBallCircle>();
        m_countDuyetXungQuanh = 0;
        //
        m_listShow.Add(ballCircle);
        for (int i = 0; i < m_listShow.Count; i++)
        {
            DuyetXungQuanhShowAll(m_listShow, m_listShow[i], lastCount);
        }
        if (m_listShow.Count > 1)
        {
            for (int i = 0; i < m_listShow.Count; i++)
            {
                m_listShow[i].ShowBall(m_listShow[i], true);
            }
        }else
        {
            m_listShow[0].ShowBall(m_listShow[0], true);
        }

       
        

    }
    private void DuyetXungQuanhShowAll(List<ObjectBallCircle> listShow, ObjectBallCircle ballCircle, int lastCount)
    {
        
        if (m_countDuyetXungQuanh + 1 > lastCount)
            return;
        //Debug.Log(count + "==============count");
      
        if (ballCircle.m_objRight != null && !m_listShow.Contains(ballCircle.m_objRight))
            m_listShow.Add(ballCircle.m_objRight);
        if (ballCircle.m_objLeft != null && !m_listShow.Contains(ballCircle.m_objLeft))
            m_listShow.Add(ballCircle.m_objLeft);
        if (ballCircle.m_objTopLeft != null && !m_listShow.Contains(ballCircle.m_objTopLeft))
            m_listShow.Add(ballCircle.m_objTopLeft);
        if (ballCircle.m_objTopRight != null && !m_listShow.Contains(ballCircle.m_objTopRight))
            m_listShow.Add(ballCircle.m_objTopRight);
        if (ballCircle.m_objBotLeft != null && !m_listShow.Contains(ballCircle.m_objBotLeft))
            m_listShow.Add(ballCircle.m_objBotLeft);
        if (ballCircle.m_objBotRight != null && !m_listShow.Contains(ballCircle.m_objBotRight))
            m_listShow.Add(ballCircle.m_objBotRight);
        //
        //Debug.Log(m_listShow.Count + "=============================");
        m_countDuyetXungQuanh++;
    }
    private void Tinh(int row, int index, int countIndex, int i, int rowLast) // i == vi tri trong list
    {
        ObjectBallCircle objLeft = null;
        ObjectBallCircle objRight = null;
        ObjectBallCircle objTopLeft = null;
        ObjectBallCircle objTopRight = null;
        ObjectBallCircle objBotLeft = null;
        ObjectBallCircle objBotRight = null;
        if (row < 2) // dong dau tien
        {
            // check phan tu dau tien // botLeft = botRight = Right = 0
            if(index < 2)
            {
                objLeft = m_listBalls[i + 1];
                if(countIndex == 10)
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                    objTopRight = m_listBalls[i + countIndex];
                }
                else
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                }
               
            }
            // check phan tu cuoi
            else if(index == countIndex) //  left = 0
            {
                objRight = m_listBalls[i - 1];
                if(countIndex == 10) //botLeft = botRight = 0
                {
                    objTopRight = m_listBalls[i + countIndex];
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                }
                else
                {
                    objTopRight = m_listBalls[i + countIndex];
                }
              
              

            }
            else // check cac phan tu con lai
            {
                objLeft = m_listBalls[i + 1];
                objRight = m_listBalls[i - 1];
                if (countIndex == 10)
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                    objTopRight = m_listBalls[i + countIndex];
                }
                else
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                }
            }

        }
        else if(row  == rowLast) // dong cuoi
        {
            // check phan tu dau tien
            if(index < 2)
            {
                objLeft = m_listBalls[i + 1];
                if(countIndex == 10)
                {
                    objBotLeft = m_listBalls[i - countIndex ];
                    objBotRight = m_listBalls[i - 1 - countIndex];
                }
                else
                {
                    objBotLeft = m_listBalls[i - countIndex + 1];
                }
            }
            else if(index == countIndex)// check phan tu cuoi
            {
                objRight = m_listBalls[i - 1];
                if (countIndex == 10)
                {
                    objBotLeft = m_listBalls[i - countIndex];
                    objBotRight = m_listBalls[i - 1 - countIndex];
                }
                else
                {
                    objBotRight = m_listBalls[i - countIndex];
                }
            }
            // check cac phan tu con lai
            else
            {
                objRight = m_listBalls[i - 1];
                objLeft = m_listBalls[i + 1];

                if (countIndex == 10)
                {
                    objBotLeft = m_listBalls[i - countIndex];
                    objBotRight = m_listBalls[i - 1 - countIndex];
                }
                else
                {
                    objBotLeft = m_listBalls[i - countIndex + 1];
                    objBotRight = m_listBalls[i - countIndex];
                }

            }
           
        }
        else // 1 < row < rowLast
        {
            // check phan tu dau tien
            if(index < 2)
            {
                objLeft = m_listBalls[i + 1];
                if(countIndex == 10)
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                    objTopRight = m_listBalls[i  + countIndex];
                    objBotLeft = m_listBalls[i  - countIndex];
                    objBotRight = m_listBalls[i  - countIndex - 1];
                }else
                {
                    objTopLeft = m_listBalls[i + countIndex];
                    objBotLeft = m_listBalls[i - countIndex + 1];
                }
            }
            else if(index == countIndex)// check phan tu cuoi
            {
                objRight = m_listBalls[i - 1];
                if (countIndex == 10)
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                    objTopRight = m_listBalls[i + countIndex];
                    objBotLeft = m_listBalls[i - countIndex];
                    objBotRight = m_listBalls[i - countIndex - 1];
                }
                else
                {
                    objTopRight = m_listBalls[i + countIndex -1 ];
                    objBotRight = m_listBalls[i - countIndex ];
                }
            }
            else // check cac phan tu con lai
            {
                objRight = m_listBalls[i - 1];
                objLeft = m_listBalls[i + 1];
                if(countIndex == 10)
                {
                    objTopLeft = m_listBalls[i + 1 + countIndex];
                    objTopRight = m_listBalls[i + countIndex];
                    objBotLeft = m_listBalls[i - countIndex];
                    objBotRight = m_listBalls[i - countIndex-1];
                }
                else
                {
                    objTopLeft = m_listBalls[i  + countIndex];
                    objTopRight = m_listBalls[i + countIndex - 1];
                    objBotLeft = m_listBalls[i - countIndex + 1];
                    objBotRight = m_listBalls[i - countIndex];
                }
               

            }
        }
        m_listBalls[i].InitNextObj(objLeft, objRight, objTopLeft, objTopRight, objBotLeft, objBotRight,row,index, countIndex, i, rowLast);//(int row, int index, int countIndex, int i, int rowLast) // i == vi tri trong list
    }
}
