using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DrawLine : MonoBehaviour
{
    [SerializeField] private ObjectBall m_prefabsBall;
    [SerializeField] private LineRenderer m_lineRenderer;
    [SerializeField] private Camera m_camera;
    [SerializeField] private Transform m_pointStart;
    [SerializeField] private LayerMask m_mask;
    //public int TotalBounce = 3;
    //public float LineOffset = 0.1f;
    private List<RaycastHit2D> m_listRays;
    public List<Vector3> m_listPaths;
    private bool m_isDraw = true;
    private Vector3 m_toPointPath = Vector3.zero;// vi tri thay the sau cung cua m_listPaths
    public Vector3 m_posLastByListPath = Vector3.zero;// vi tri cuoi cung cua m_listPaths khi chua gui event
    //private bool m_isRayBall = false;
    public ObjectBallCircle m_ballRay = null;
    private ObjectBall m_currenBall = null;
    public ObjectBallCircle m_BallCircleNext = null;
    private bool m_isCreateBall = true;
    private float m_lengthDrawLine  = 0;
    private void Awake()
    {
        Application.targetFrameRate = 60;
        GameController.getPositionBall += GetPosition;
    }
    private void OnDestroy()
    {
        GameController.getPositionBall -= GetPosition;
    }

    void Update()
    {
        CreateBall();
        CheckRayCast();
        if (Input.GetMouseButtonUp(0))
        {
            m_isDraw = false;
            MoveBall();
        }
       
    }
    private void CreateBall()
    {
        if (m_isCreateBall)
        {
            m_isCreateBall = false;
            ObjectBall obj = Instantiate(m_prefabsBall);
            obj.transform.position = m_pointStart.transform.position;
            obj.gameObject.SetActive(true);
            //
            m_currenBall = obj;
        }
    }   
    private void CheckRayCast()
    {
     
        if (!m_isDraw)
            return;
        RaycastHit2D ray;
        Vector2 direction = Vector2.zero;
        //
        m_ballRay = null;
        m_listRays = new List<RaycastHit2D>();
       
        direction = (m_camera.ScreenToWorldPoint(Input.mousePosition) - m_pointStart.transform.position).normalized;
        //
        float angle = Mathf.Atan2(direction.x, direction.y);
        //Debug.Log("==========" + angle);
        if (angle < -1.4 || angle > 1.4) // gioi han goc duoc phep ve Draw line
        {
            m_listRays.Clear();
            return;
        }
        //
        ray = Physics2D.Raycast(m_pointStart.transform.position, direction,m_mask);
        //Debug.DrawRay(m_pointStart.transform.position, direction * ray.distance, Color.green);
        m_listRays.Add(ray);
        if (ray.collider == null)
            return;
        if (ray.collider.tag == "ball")
        {

            ObjectBallCircle ball = ray.collider.GetComponent<ObjectBallCircle>();
            if (ball == null)
                return;
            m_ballRay = ball;
            // m_isRayBall = false;
            //Debug.Log("ray.collider.tag =====================");
        }
        if (ray.collider.tag == "boder")
        {
            Vector2 newStart = Vector2.zero;
            Vector2 newRayPos = ray.point;
            while (ray.collider.tag == "boder")
            {

                newStart = new Vector2(newRayPos.x, newRayPos.y);
                direction = Vector2.Reflect(direction, Vector2.right);

                //I had to put in a tolerances so a new raycast could start,
                //there is likely a better way, feedback appreciated.
                Vector2 rightTol = new Vector3(-.001f, 0);
                Vector2 leftTol = new Vector3(.001f, 0);
                if (ray.collider.name == "wallRight")
                {
                    ray = Physics2D.Raycast(newStart + rightTol, direction, m_mask);

                }
                else //if (ray.collider.name == "wallLeft")
                {
                    ray = Physics2D.Raycast(newStart + leftTol, direction, m_mask);
                }
                if (ray.point == Vector2.zero)
                {
                    //Debug.Log("=============Zero");
                    m_listRays.Clear();
                    break;
                }
                if (ray.collider.tag == "ball")//if (ray.collider.name == "ball")
                {
                    ObjectBallCircle ball = ray.collider.GetComponent<ObjectBallCircle>();
                    if (ball == null)
                        return;
                    m_ballRay = ball;
                    m_listRays.Add(ray);
                }
                else
                {
                    m_listRays.Add(ray);
                    newRayPos = ray.point;
                }
              
            }
           
        }
        DrawRayLine();
    }
    private void DrawRayLine()
    {
        //if (m_posLastByListPath == (Vector3)m_listRays[m_listRays.Count - 1].point) // dang vuong cho nay
        //    return;
        // Draw Line by position
        m_lengthDrawLine = 0;
        m_listPaths = new List<Vector3>();
        Vector3 temp = Vector3.zero;
        //
        if (!m_isDraw)
            return;

        if (m_listRays.Count > 0)
        {
            m_lineRenderer.positionCount = m_listRays.Count + 1; // set Size LineRenderer
            m_lineRenderer.SetPosition(0, m_pointStart.transform.position);
            m_listPaths.Add(new Vector3(m_pointStart.transform.position.x, m_pointStart.transform.position.y, 0));
            Vector2 _rayPos;
            for (int i = 0; i < m_listRays.Count; i++)
            {
                _rayPos = m_listRays[i].point;
                m_lengthDrawLine += m_listRays[i].distance;
                m_lineRenderer.SetPosition(i + 1, _rayPos);
                m_listPaths.Add(new Vector3(_rayPos.x, _rayPos.y, 0));//Add Toa do path ball
                if( i == m_listRays.Count-1)
                {
                    temp = new Vector3(_rayPos.x, _rayPos.y, 0);
                    //
                    m_posLastByListPath = (Vector3)m_listRays[i].point; // luu vi tri cuoi cung
                }
            }
            GameController.CheckPositionBall(false, temp, m_currenBall, m_ballRay);
        }
 
    }
   
    private void GetPosition(Vector3 pos, ObjectBallCircle ballCircle, ObjectBall ball)
    {
        m_BallCircleNext = ballCircle;
        //float timeMove = m_lengthDrawLine / 7f;
        m_toPointPath = pos;
        //
        //Debug.Log(m_listPaths.Count + "==================sau khi check");
        int count = m_listPaths.Count - 1;
        m_listPaths.RemoveAt(count);
        m_listPaths.Add(m_toPointPath);
        if (m_toPointPath != Vector3.zero) // check vi tri cuoi da update chua
        {
            m_listPaths.RemoveAt(count);
            m_listPaths.Add(m_toPointPath);
        }

    }
    private void MoveBall()
    {
        if (m_isDraw)
            return;
        Vector3[] array = m_listPaths.ToArray();

        // reset line
        m_listPaths.Clear();
        m_listRays.Clear();
        m_lineRenderer.positionCount = 0;
        m_toPointPath = Vector3.zero;
        m_posLastByListPath = Vector3.zero;

        // Move ball
        float timeMove = m_lengthDrawLine / 7f;
        m_currenBall.gameObject.SetActive(true);
        m_currenBall.transform.DOPath(array, timeMove).SetEase(Ease.Linear).OnComplete(() => {
            StartCoroutine(WaitComplete());
        });
    }
    IEnumerator WaitComplete()
    {
        m_currenBall.transform.SetParent(m_BallCircleNext.transform);
        yield return new WaitForSeconds(0.01f);
        m_BallCircleNext.SetBall(m_BallCircleNext, m_currenBall, m_currenBall.GetTypeBall(), true);
        m_BallCircleNext.ShowBall(m_BallCircleNext, false);
        yield return new WaitForSeconds(0.1f);
        m_isDraw = true;
        m_isCreateBall = true;
       
    }
}
