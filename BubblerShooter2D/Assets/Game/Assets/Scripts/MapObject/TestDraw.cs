using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDraw : MonoBehaviour
{
    public Camera m_cam;
    public LineRenderer m_lineRenderer;
    private List<Vector2> rayLists;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
      
        
        rayLists = new List<Vector2>();
        rayLists.Add(transform.position);
        Vector3 mousePos = m_cam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = mousePos - transform.position;
        Vector3 directionReflect = direction;
        //
        Vector3 posStart = transform.position;
        RaycastHit2D ray = Physics2D.Raycast(transform.position, direction);
        Vector3 posNext = ray.point;
        rayLists.Add(ray.point);
       
        if (ray.collider.tag == "boder")
        {
            while (ray.collider.tag == "boder")
            {
                posNext = ray.point;
                direction = posNext - posStart;
                directionReflect = Vector3.Reflect(direction.normalized, Vector3.right);
                if (ray.collider.name == "wallLeft")
                {
                    Vector3 addLeft = (Vector2)posNext + new Vector2(0.01f, 0);
                    posStart = ray.point;
                    ray = Physics2D.Raycast(addLeft, directionReflect);
                }
                else
                {
                    Vector3 addRight = (Vector2)posNext - new Vector2(0.01f, 0);
                    posStart = ray.point;
                    ray = Physics2D.Raycast(addRight, directionReflect);
                }
                //
                rayLists.Add(ray.point);
                if (ray.point == Vector2.zero)
                {
                    Debug.Log("=============Zero");
                    break;
                }
            }
               


            //
            m_lineRenderer.positionCount = rayLists.Count;

            for (int i=0;i < rayLists.Count; i++)
            {
                m_lineRenderer.SetPosition(i, rayLists[i]);
            }

        }
    }
}
