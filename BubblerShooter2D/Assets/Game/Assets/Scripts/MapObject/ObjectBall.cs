using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class ObjectBall : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    [SerializeField] private List<Sprite> m_listSprite;
    [SerializeField] private TypeColorBall m_typeColorBall = TypeColorBall.empty;
    //[SerializeField] private Collider2D m_collider;
    public bool m_isCurrent = false;
    //public bool m_isBall = false;

    public List<ObjectBall> m_listNextBall;
    public bool m_BallTaoTruoc = false;
    private TypeColorBall[] m_arrayColors =  { TypeColorBall.blue, TypeColorBall.green};
    private void Awake()
    {
       
        if (!m_BallTaoTruoc)
        {
            int rand = Random.RandomRange(0, 2);// Random.RandomRange(0, m_listSprite.Count);
            m_typeColorBall = m_arrayColors[rand];
            Init(m_arrayColors[rand]);
        }
    }
   
  
    public void Init(TypeColorBall color)
    {
        m_BallTaoTruoc = true;
        m_typeColorBall = color;
        ChangeColor(color);
    }  
    //public bool GetIstBall()
    //{
    //    return m_isBall;
    //}
    public TypeColorBall GetTypeBall()
    {
        return m_typeColorBall;
    }
    private void ChangeColor(TypeColorBall color)
    {
        //Debug.Log(color + "=================ball");
        //switch(color)
        //{
        //    case TypeColorBall.blue:
        //        //m_typeColorBall = TypeColorBall.blue;
        //        break;

        //    case TypeColorBall.red:
        //        //m_typeColorBall = TypeColorBall.red;

        //        break;
        //    case TypeColorBall.green:
        //        //m_typeColorBall = TypeColorBall.green;

        //        break;
        //    case TypeColorBall.yellow:
        //        //m_typeColorBall = TypeColorBall.yellow;

        //        break;
        //    case TypeColorBall.violet:
        //        //m_typeColorBall = TypeColorBall.violet;

        //        break;
        //}
        if ((int)color < 0)
            return;
        m_spriteRenderer.sprite = m_listSprite[((int)color)];
    }
}
[SerializeField] 
public class NextPos
{
    public int row = 0;
    public int index = 0;
    public int indexLeft = 0;
    public int indexRight = 0;
    public int indexTopRight = 0;
    public int indexTopLeft = 0;
    public int indexBotRight = 0;
    public int indexBotLeft = 0;
}
